%Observação importante: para usar os recursos da classe ABNTEX, é necessário fazer o download de sua biblioteca,
%visto que a mesma não é nativa do ambiente \LaTeX.
%Em um distribuição linux baseada em debian o pacote pode ser instalado pelo comando: sudo apt-get install abntex.
%Autor:Jean Henrique Ferreira Freire

\documentclass{abnt}
\usepackage[brazil]{babel}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[num]{abntcite}
\usepackage{graphicx}
\usepackage{url}
\usepackage{listings}
\usepackage{color}
\usepackage{tabulary}
\usepackage[dvipsnames]{xcolor}
\usepackage{array}
\usepackage{amsmath}
\usepackage{multirow}

\lstset{%
  language=HTML,
  numbers=left,
  basicstyle=\small\ttfamily,
  breaklines=true,
  columns=fullflexible,
  frame=single,
  frameround=tttt,
  showstringspaces=false,
  identifierstyle=\color{magenta!60!black},
  stringstyle=\color{blue}
}

\begin{document}
\autor{Diogo Marques Santana}

\titulo{Coleta Contínua de Dados da World Wide Web}

\orientador[Orientador:\\]{Wagner Meira Jr. - Departamento de Ciência da Computação}

\comentario{Apresentado como requisito da disciplina de Monografia em Sistemas de Informação do DCC/UFMG}

\instituicao{Universidade Federal de Minas Gerais \par Instituto de Ciências
Exatas \par Departamento de Ciência da Computação}

\local{Belo Horizonte} \data{2015/2}

\capa 
\folhaderosto 


\begin{resumo}
A World Wide Web contem uma quantidade significativa do conhecimento global, constituindo uma fonte de informação de interesse de diversas instituições, empresas,
do governo, além dos cidadões comuns. Coletar a Web é notavelmente algo revelante para diversos indivíduos considerando diferentes contextos como
pesquisa de dados, análise de conteúdo e tomada de decisões. Contudo, a Web apresenta dificuldades inerentes de seu imensurável tamanho e de seu caráter de frequente
mudança. Apesar de web crawlers como o Heritrix apresentarem boas soluções para coletar domínios grandes, há pouco desenvolvido no sentido de manter
os dados consistentes com as atualizações recorrentes na Web durante a coleta. O objetivo desse projeto é propor uma metodologia de coleta que 
mantem as páginas já visitadas atualizados durante a coleta, ao mesmo tempo em que coleta novas páginas.

\textbf{Palavras-chaves}: web crawler, coleta contínua, Heritrix, World Wide Web.
\end{resumo}

\sumario %comando que gera o sumário automaticamente
\renewcommand*\listfigurename{LISTA DE FIGURAS}
\listoffigures %comando que gera um sumário para a lista de figuras do texto automaticamente



\chapter{INTRODUÇÃO}
\section{Contexto}

% Evolução da Web e necessidade de indexação e coleta de seus dados
Desde o surgimento da World Wide Web no início dos anos 90, a mesma vem mostrando um ritmo surpreendente de crescimento. A grande quantidade e
diversificação de seu conteúdo gerou taxas de crescimento exponenciais que tornou imprescindível o uso de mecanismos de busca para explorar a Web \cite{sigurdhsson}.

% Web crawler, indexação e coleta
O processo de exploração realizado pelos mecanismos de busca normalmente utilizam \textit{crawlers}, scripts automatizados que navegam independentemente pela
Web \cite{fica} que são utilizados para indexar as páginas descobertas para futura consulta. Contudo, visto que a Web está mudando constantemente,
torna-se interessante não somente indexá-la, mas também coletá-la.

% Explicar os desafios em uma coleta
Uma coleta abrange operações como localização, obtenção, armazenamento e análise de dados de um determinado contexto da Web, 
o qual compreende um conjunto de domínios e subdomínios, incluindo seus sítios e suas respectivas páginas. Coletar toda a Web 
é praticamente impossível, visto seu imensurável tamanho e seu caráter dinâmico. Portanto, é interessante considerar restringir o contexto 
da coleta, seja limitando seu escopo para determinados domínios e/ou selecionando páginas de acordo com algum critério \cite{sigurdhsson}.

A coleta de contextos pequenos é, em geral, uma tarefa simples para as diversas ferramentas e coletores disponíveis atualmente. Contudo, coletas extensas 
que requerem meses para cobrir todo o escopo apresentam dificuldades em manter seus dados atualizados. Visto o caráter dinâmico da Web, a mudança frequente
de dados é algo esperado, o que pode ser exemplificado por páginas que são atualizadas diariamente. Logo, um longo período de coleta pode acarretar inconsistências
entre os dados no memento em que foram coletados e os dados reais ao fim da coleta.

% Coverage x freshness
Podemos notar que a cobertura do escopo e manter os dados atualizados são interesses conflitantes. Focar recursos para coletar páginas ainda não visitadas significa
comprometer recursos em recoletar páginas para garantir sua consistência e vice-versa. Um bom coletor deve balancear ambos os interesses, de modo a coletar conteúdo de
valor significativo, evitar dados de baixa qualidade e redundantes, além de acompanhar parte das mudanças da Web \cite{olston}.

% Heritrix
Dos coletores disponíveis atualmente, o Heritrix \cite{heritrix} foi escolhido como objeto de estudo, crawler \textit{open source} \cite{opensource} desenvolvido pelo 
\textit{Internet Archive} (IA) seguindo os interesses do \textit{International Internet Preservation Consortium} (IIPC), associação formada por diversas 
Bibliotecas Nacionais juntamente com o IA que tem como missão "adquirir, preservar e tornar acessível conhecimento e informação da Internet para futuras 
gerações em todos os lugares, promovendo trocas e relações internacionais" \cite{IIPC}. 

A estratégia de coleta utilizada pelo Heritrix pode ser descrita pela estratégia de \textit{snapshot}: uma vez descoberta, uma URL não é
revisitada; quando redescoberta, é descartada como duplicada \cite{sigurdhsson}. Apesar do Heritrix ser uma ferramenta robusta para cobrir 
escopos extensos, a única forma de atualizar um dado já coletado é aguardar a finalização da coleta para repetí-la. Portanto, a estratégia 
presente é pouco efetiva para capturar mudanças de conteúdo pelo tempo, ou seja, imprópria para realizar uma \textit{coleta contínua}, a qual
preocupa-se em manter os dados atualizados.

% Figura snapshot x continua?

\section{Objetivos}

Este trabalho de monografia propõe uma estratégia de coleta contínua (ou incremental) a ser integrada ao web crawler Heritrix, de modo a
dividir recursos entre coletar novas páginas descobertas e recoletar páginas periodicamente.

De modo a realizar tal tarefa, um módulo a ser integrado ao Heritrix foi implementado para tratar da atualidade dos dados, enquanto o Heritrix
continua por tomar conta da cobertura do escopo. Este módulo possui como responsabilidades:

\begin{itemize}
 \item indicar se uma página foi atualizada ou não;
 \item definir uma máquina de estados que reflita o ciclo de vida de uma página de acordo com a resposta de uma requisição HTTP;
 \item e escalonar a recoleta de uma página de acordo com seu estado atual e refletindo sua real frequência de atualização.
\end{itemize}

Além disso, é necessário um estudo mais aprofundado do Heritrix de modo a permitir a compreensão de seu funcionamento, como configurá-lo
corretamente e de forma eficiente, como integrar tal módulo ao Heritrix e, por fim, possíveis mudanças que seriam necessárias ao código original
para adequa-lo a uma estratégia de coleta contínua.

Por fim, pretende-se calcular indicadores para cada versão de dados coletados, tomando como estudo de caso o trabalho do CEPTRO.br\footnote{Área do NIC.br 
(Núcleo de Informação e Coordenação do Ponto BR) responsável por serviços e projetos relacionados principalmente à infraestrutura da Internet no Brasil 
e ao seu desenvolvimento.} sobre os Estudos da Web .br, o qual coleta dados da Web brasileira e realiza diversas análises sobre esses dados, como tipo 
de tecnologias utilizadas, suporte IPV6, servidores utilizados e outros. Contudo, o universo de coleta será limitado para os domínios 
que representam os ministérios do governo brasileiro.


\chapter{CONTEXTUALIZAÇÃO E TRABALHOS RELACIONADOS}

\section{Referencial Teórico}

\subsection{Web Crawler}

Também conhecido como \textit{robô} ou \textit{spider}, é um programa que navega sistematicamente pela Internet. Seu algoritmo funciona da seguinte forma: dado
um conjunto semente (\textit{seed}) de \textit{Uniform Resource Locators} (URLs), o crawler coleta todas as páginas endereçadas pelas URLs, extrai os hyperlinks
contidos nas páginas e iterativamente coleta as páginas endereçadas por estes hyperlinks. Apesar de ser um algoritmo simples, crawler encontram
diversos desafios \cite{olston}:

\begin{itemize}
 \item \textbf{Escalabilidade:} uma vez que a Web é extremamente grande e dinâmica, crawlers que buscam uma alta cobertura de domínio e atualidade de dados
 precisam ter uma alta taxa de transferência de dados.
 \item \textbf{Seleção de conteúdo:} mesmo os crawlers de mais alta taxa de transferência não são capazes de coletar toda a Web; portanto, torna-se importante
 selecionar o conteúdo que seja mais relevante, ignorando dados de baixa qualidade, redundantes ou maliciosos.
 \item \textbf{Obrigações sociais:} crawlers devem tomar precauções para não sobrecarregar os servidores com suas requisições, além de seguir as restrições
 impostas por cada servidor contidos no arquivo \textit{robots.txt}, localizado na raiz da aplicação Web.
 \item \textbf{Adversários:} diz respeito tipos de conteúdo que visam desencaminhar para contextos de outra natureza, geralmente motivado por fins financeiros,
 como anúncios comerciais.
\end{itemize}

\subsection{Heritrix}

Web crawler de código livre (open source) desenvolvido pelo Internet Archive utilizando a linguagem Java e bastante robusto, apresentando alta escalabilidade,
respeitando suas obrigações sociais, além de permitir um alto nível de detalhamento em configurações que permitem uma precisa seleção de conteúdo - seja por tipo,
domínio ou padrão na URL - e evita o redirecionamento malicioso de escopo.

Os elementos principais da arquitetura do crawler é descrita pela figura~\ref{fig:heritrix_arch}. Em relação ao controlador do crawler, a lista inicial de semente é alocada 
para a fronteira, a qual monitora uma fila de URLs que estão escalonadas para serem coletadas. Em seguida, uma URL da fila é alocada para uma thread, a qual é processada em 
sequência pela cadeia de processadores, composto por diversos processadores que executam ações específicas e ordenadas para a URL em questão, analizando os resultados 
retornados e passando as novas URLs descobertas - extraidas da URL original - para a fronteira. Por fim, o processo é repetido para as novas URLs descobertas.

Em relação aos demais componentes, o console administrativo web permite a configuração e controle de uma coleta. Em contrapartida, o ordenador do crawler instancia e guarda
referências para todos os módulos configurados do controlador do crawler.

\begin{figure}[ht!]
  \centering
    \includegraphics[scale=0.38]{imagens/heritrix_arch.jpg}
    \caption{Arquitetura simplificada do Heritrix}
    \label{fig:heritrix_arch}
\end{figure}

O Heritrix foi desenvolvido de forma a ser capaz de realizar os seguintes tipos de coleta~\cite{heritrix}:

\begin{itemize}
 \item \textbf{Coleta ampla:} coleta extensa, de alta taxa de transferência onde cada página do escopo é importante. O objetivo principal é a cobertura de todo o escopo.
 \item \textbf{Coleta focada:} coleta pequena ou média - geralmente menor que 10 milhões de documentos - que visa a cobertura completa de determinados sitios ou tópicos.
 \item \textbf{Coleta contínua:} revisita páginas previamente coletadas à procura de mudanças, ao mesmo tempo que descobre e coleta novas páginas, além de adaptar a taxa
 de revisitação de acordo com a frequência de mudança da página.
 \item \textbf{Coleta experimental:} inclui técnicas atípicas de coleta que utilizam diferentes protocolos, ordenação de recursos e outros métodos.
\end{itemize}

Os dois primeiros tipos de coleta utilizam a estratégia de snapshot: uma "foto" do estado atual da URL é guardada e, uma vez descoberta, a mesma não é revisitada.
Entretanto, não existem soluções implementadas no Heritrix para uma coleta contínua diferentes de repetir a estratégia de snapshot após o fim da coleta. É notável que
esta estratégia, apesar de ser eficiente para a coleta ampla e focada, não se adequa a uma coleta contínua, uma vez que as páginas não são revisitadas durante a coleta e,
portanto, atualizações não são detectadas até a próxima coleta, o que é um problema para coletas extensas, que podem demorar meses.

\subsection{Detecção de mudança da página}

Estima-se que 60\% da Web é dinâmica~\cite{cho2003} e, portanto, são grandes a chances de uma página coletada se tornar obsoleta até o final de uma coleta. Uma coleta
contínua exige que sejam gastos o mínimo de recursos neste processo, de modo a não compremeter em menor grau a cobertura do domínio. Portanto, é interessante inferir se
a página sofreu mudanças ou não, de modo a escalonar a recoleta da página de uma maneira que melhor reflita a frequência de atualização da mesma.

Os algoritmos de detecção de mudança encontrados na literatura consideram, principalmente, mudanças na estrutura da página - nas tags HTML, no conteúdo texto e nas
imagens - em hyperlinks ou como parte da página~\cite{yadav2008}. Neste trabalho serão focados apenas os dois primeiros tipos de mudanças.


\section{Trabalhos Relacionados}

\subsection{Adaptative Revisiting with Heritrix}

Tese de mestrado~\cite{sigurdhsson} que propõe um método para coletar partes da Web que mudam frequentemente a partir de uma estratégia contínua de
coleta, a qual foi implementada como uma parte do Heritrix.


\chapter{DESENVOLVIMENTO DO TRABALHO}

\section{Decisões de projeto}

A primeira parte do desenvolvimento dedicou a estudar o problema, resultando em modelos de como implementar as partes propostas que serviriam como solução para o problema
apontado. Por fim, o problema se dividiu nas alterações a serem implementadas dentro e fora do Heritrix.

\subsection{Modificações no Heritrix}

O primeiro passo para implementar uma coleta contínua no Heritrix é alterar com o crawler lida com o módulo da fronteira, de modo a evitar que a coleta pare quando
todas as filas de URLs da fronteira estejam vazias. Uma vez que sempre existe a verificação por atualizações de páginas, então é desejável que a coleta dure por tempo
indefinido.

Uma das formas de solucionar este problema seria implementar outro módulo de fronteira. Contudo, segundo Sigur{\dh}sson, é mais viável alterar o controlador
da coleta por questões de simplicidade~\cite{sigurdhsson}. A última solução também acarreta no controle direto das sementes direcionadas à fronteira, item extremamente
útil uma vez que URLs serão reescalonados frequentemente para atualizar os dados coletados.

\subsection{Módulo externo ao Heritrix}

O módulo a ser integrado ao Heritrix deve ser capaz de providenciar ao controlador da coleta a lista de URLs a ser coletada. Nesta lista, cada URL deve ser escalonada
de acordo com os seguintes fatores:

\begin{itemize}
 \item \textbf{Resultado da coleta:} deve-se dar menos prioridade a páginas que retornam algum erro de coleta com frequência, enquanto devem ser priorizadas páginas
 que geralmente são coletadas com sucesso.
 \item \textbf{Frequência de atualização:} páginas atualizadas com frequência devem ser revisitadas mais vezes, enquanto páginas de caráter mais estáticos 
 devem ser pouco revisitadas.
\end{itemize}

\subsection{Ciclo de vida da página}

O resultado da coleta de uma página pode ser intuitivamente representado por uma máquina de estados: a página a ser coletada sai do conjunto semente e
inicia no estado \textit{Descoberta} e, de acordo com a resposta da requisição HTTP de tentativa de coleta, a página alterna entre os estados \textit{Ativa} e
\textit{Inativa} (~\ref{fig:ciclo_pag}).

\begin{figure}[ht!]
  \centering
    \includegraphics[scale=0.38]{imagens/ciclo_pag.jpg}
    \caption{Ciclo de vida de uma página Web}
    \label{fig:ciclo_pag}
\end{figure}

Os estados variam de acordo com as seguintes ações:

\begin{itemize}
 \item \textbf{(Re)Coleta com sucesso:} a requisição à página retornou com sucesso, tipicamente o código HTTP 2XX (e.g: 200 - OK).
 \item \textbf{(Re)Coleta com erro:} a requisição à página retornou erro, tipicamente o código HTTP 4XX e 5XX (e.g: 404 - Não encontrado; 500 - Erro interno de servidor).
\end{itemize}

\subsection{Função de escalonamento}

A função de escalonamento de revisita da página foi definida visando otimizar os recursos computacionais gastos na coleta. Vale ressaltar que uma função muito agressiva 
pode resultar na saturação do sistema, impossibilitando a coleta de todas as páginas no tempo proposto, enquanto uma função muito conservadora pode resultar em ociosidade
do sistema. Sabendo-se o último estado da página e se a mesma foi modificada ou não, é possível definir uma função de escalonamento que reflita melhor a realidade das páginas
na tentativa de explorar a localidade de referência temporal dos dados. Em outras palavras, páginas que mudaram recentemente tem maior probabilidade de mudar novamente,
sendo assim escalonadas mais vezes, enquanto páginas que não apresentaram mudanças tentem a continuarem iguais, reduzindo-se assim seu tempo de escalonamento.

Desta forma, foi definido o tempo base e mínimo ($t$) de escalonamento como 1 dia, onde $t$ pode dobrar ou reduzir pela metade - desde que seja maior ou igual ao tempo base -
seguindo a tabela~\ref{table:scheduler}, que indica o estado resultante e o tempo em que a página será escalonada.

\newcolumntype{L}[1]{>{\raggedright\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}
\newcolumntype{C}[1]{>{\centering\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}
\newcolumntype{R}[1]{>{\raggedleft\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}

\begin{table}[!h]
\centering
\normalsize
    \begin{tabulary}{1.0\textwidth}{| C{1.5cm} | C{3cm} | C{3cm} | C{3cm} |}
     \hline
     {} & \multicolumn{2}{c|}{\textbf{Coleta com sucesso}} & \textbf{Coleta com erro} \\ \hline
     {} & \textbf{Modificada} & \textbf{Não modificada} & -\\ \hline
     \textbf{Ativa} & Ativa($\frac{t}{2}$) & Ativa($2\times t$) & Inativa($\frac{t}{2}$)\\ \hline
     \textbf{Inativa} & Ativa($\frac{t}{2}$) & Ativa($2\times t$) & Inativa($2\times t$)\\ \hline
    \end{tabulary}
\caption{Escalonador de páginas}
\label{table:scheduler}
\end{table}

\subsection{Detecção de mudança da página}

Definir uma estratégia de detecção de mudança de página pode não ser tão trivial. Para definí-la, devemos considerar o que é aceito com uma mudança de acordo
com o contexto da aplicação, fatores que caracterizam mudanças em uma página, quanto tempo estamos dispostos a gastar para detectar a mudança e qual é a 
quantidade de erro aceitável no algoritmo.

Atualizações ínfimas de conteúdo alterações de data ou de relógio (figura~\ref{fig:page_clock}) não são, em geral, de extrema relevância para a coleta. Portanto,
foi definido neste trabalho que atualizações menores que 10\% da página não seriam considerados como mudanças de página.

\begin{figure}[ht!]
  \centering
    \includegraphics[scale=0.38]{imagens/page_clock.jpg}
    \caption{Mudanças de relógio em uma mesma página.}
    \label{fig:page_clock}
\end{figure}

O problema agora se resume em definir os fatores que caracterizam mudanças em uma página e que podem ser calculados rapidamente. Desse modo, definiu-se que o primeiro
passo do algoritmo deveria buscar por indicadores de mudança antes de coletar a página, o que pode ser realizado pela verificação dos seguintes itens do cabeçalho da página:

\begin{itemize}
 \item \textbf{Last-modified:} Última modificação da página. Caso a data seja maior que a da última coleta, então a pagína foi modificada.
 \item \textbf{Content-Length:} Tamanho da página. Caso seja diferente em mais de 500 bytes que o da última coleta, então houve atualização da página.
 O número de bytes em questão segue a mesma ideia de desconsiderar mudanças ínfimas na página. Tamanhos iguais a zero são desconsiderados por apresentarem
 confiabilidade muito baixa.
\end{itemize}

Apesar de tais indicadores não serem, na prática, completamente confiáveis e presentes no cabeçalho das páginas, sua utilização acarreta em boa redução de uso de
recursos, tornando aceitável a taxa de erro.

Caso não seja possível usar os indicadores acima, então não há outra opção senão coletar a página. Contudo, mudanças ainda podemos evitar estatégias que necessitem ler,
comparar ou \textit{parsear} o arquivo. O passo seguinte é comparar o tamanho do arquivo com o tamanho obtido na coleta anterior, aplicando a mesma lógica utilizada no
indicador \textit{Content-Length}. Contudo, se o arquivo tem tamanho zero significa que houve um erro na coleta e, portanto, não há como detectar mudanças.

Como última instância, devemos usar estatégias que utilizem o conteúdo do arquivo. Note que um texto HTML pode ser dividido entre sua estruturação HTML e seu conteúdo puramente
texto. Dessa forma, adições e remoções de tags HTML configuram mudanças na página, que podem ser detectadas a partir do algoritmo descrito por Yadav~\cite{yadav2008}:

\begin{figure}[ht!]
  \centering
    \includegraphics[scale=0.38]{imagens/page_struct.jpg}
    \caption{Algoritmo de detecção de mudança na estrutura da página.}
    \label{fig:page_struct}
\end{figure}

\begin{itemize}
 \item definir duas strings compostas pelas tags HTML da página;
 \item a \textit{string1} possui o caracter inicial de cada tag;
 \item a \textit{string2} possui o caracter final de cada tag;
 \item caso a tag contenha somente uma letra, ela será repetida em ambas as tags.
\end{itemize}

As vantagens principais dessa técnica são a simplicidade proporcionada pelas strings, ao contrário de outros algoritmos que utilizam estruturas de dados de árvore para
guardar a estrutura da página, além da maioria das mudanças serem detectadas diretamente por comparações apenas com a primeira string.

Entretanto, caso a técnica acima não detecte mudanças na página, é possível que tenha ocorrido uma mudança apenas textual na página e, portanto, é preciso
definir uma estratégia para capturar tal situação. Apesar do método proposto por Yadav de cálcular um \textit{checksum} para o conteúdo da página ser relativamente
simples, não é possível observar mudanças sutis e ínfimas de conteúdo a partir dessa técnica. Portanto, foi escolhido o algoritmo de distância de 
Damerau-Levenshtein~\cite{bard2007}, de modo a classificar 10\% ou mais de diferença de páginas como uma atualização.

\chapter{RESULTADOS E DISCUSSÃO}

No estágio atual do projeto, apenas o algoritmo de detecção de mudança de página teve sua implementação finalizada. A base de dados de teste foi construída a
partir da coleta de 1032 páginas coletadas por 10 dias utilizando a ferramenta \textit{wget}. No total, obteve-se 9902 páginas coletadas com sucesso, que
foram verificadas com o documento coletado no dia imediatamente depois utilizando o comando \textit{diff}, sendo a saída comparada com o resultado do algoritmo.
Os resultados podem ser observados na tabela~\ref{table:results}:

\begin{table}[!h]
\centering
\normalsize
    \begin{tabulary}{1.0\textwidth}{| C{4cm} | C{1.5cm} | C{1.5cm} |}
     \hline
     \textbf{Total de páginas} & 9902 & 100\%\\ \hline
     \textbf{Páginas atualizadas} & 3709 & 37.46\%\\ \hline
     \textbf{Não atualizadas} & 6193 & 62.54\%\\ \hline
     \textbf{Falsos negativos} & 570 & 15.37\%\\ \hline
     \textbf{Falsos positivos} & 228 & 3.68\%\\ \hline
    \end{tabulary}
\caption{Validação do algoritmo de detecção de mudança de página}
\label{table:results}
\end{table}

Observe que a porcentagem de falsos negativos é dada em função do total de páginas que realmente foram atualizadas, enquanto os falsos positivos estão em
função do total de páginas que não sofreram modificações.

A taxa de erro do algoritmo mostrou-se aceitável, uma vez que são taxas de erro são baixos e são compensados em otização de recursos para o coletor.
Também foi observado que a maior parte dos falsos negativos teve como causa mudanças de conteúdo que não alteram o tamanho final da página, situação
comum em páginas principais que contém um tamanho fixo de caracteres para uma curta introdução a notícias e/ou relógios e marcadores de datas. Uma vez que o
\textit{Content-Length} ou o tamanho do arquivo da página se mantem o mesmo, o algoritmo acaba por não classificar tal situação como atualização.


\chapter{CONCLUSÕES E TRABALHO FUTUROS}

Em relação à coleta de dados Web, é um desafio desenvolver um coletor que consiga balancear a cobertura do escopo - coletando a maior parte do domínio proposto -
com a manutenção da atualidade dos dados - onde os dados tendem a se tornar desatualizados até o fim da coleta devido ao caráter dinâmico da Web.

A partir deste trabalho foi possível definir uma estratégia de coleta contínua, prezando a otimização de recursos computacionais. Apesar de sua implementação
ainda se apresentar no estágio inicial, o algoritmo de detecção de mudança da página apresentou bons resultados com um baixo número de falsos positivos e negativos.

O estágio atual de desenvolvimento do projeto encontra-se na implementação da máquina de estados da página e no escalonador. Trabalhos futuros serão focados na
integração desdes módulos ao Heritrix, de modo a controlar as URLs escalonadas para a fronteira do crawler.

% \bibliographystyle{apalike} 
\bibliography{monografia.bib}

\end{document}
