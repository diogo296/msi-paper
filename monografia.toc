\select@language {brazil}
\contentsline {chapter}{LISTA DE FIGURAS}{}
\contentsline {chapter}{\numberline {1}INTRODU\IeC {\c C}\IeC {\~A}O}{6}
\contentsline {section}{\numberline {1.1}Contexto}{6}
\contentsline {section}{\numberline {1.2}Objetivos}{7}
\contentsline {chapter}{\numberline {2}CONTEXTUALIZA\IeC {\c C}\IeC {\~A}O E TRABALHOS RELACIONADOS}{9}
\contentsline {section}{\numberline {2.1}Referencial Te\IeC {\'o}rico}{9}
\contentsline {subsection}{\numberline {2.1.1}Web Crawler}{9}
\contentsline {subsection}{\numberline {2.1.2}Heritrix}{10}
\contentsline {subsection}{\numberline {2.1.3}Detec\IeC {\c c}\IeC {\~a}o de mudan\IeC {\c c}a da p\IeC {\'a}gina}{11}
\contentsline {section}{\numberline {2.2}Trabalhos Relacionados}{11}
\contentsline {subsection}{\numberline {2.2.1}Adaptative Revisiting with Heritrix}{11}
\contentsline {chapter}{\numberline {3}DESENVOLVIMENTO DO TRABALHO}{12}
\contentsline {section}{\numberline {3.1}Decis\IeC {\~o}es de projeto}{12}
\contentsline {subsection}{\numberline {3.1.1}Modifica\IeC {\c c}\IeC {\~o}es no Heritrix}{12}
\contentsline {subsection}{\numberline {3.1.2}M\IeC {\'o}dulo externo ao Heritrix}{12}
\contentsline {subsection}{\numberline {3.1.3}Ciclo de vida da p\IeC {\'a}gina}{13}
\contentsline {subsection}{\numberline {3.1.4}Fun\IeC {\c c}\IeC {\~a}o de escalonamento}{13}
\contentsline {subsection}{\numberline {3.1.5}Detec\IeC {\c c}\IeC {\~a}o de mudan\IeC {\c c}a da p\IeC {\'a}gina}{14}
\contentsline {chapter}{\numberline {4}RESULTADOS E DISCUSS\IeC {\~A}O}{17}
\contentsline {chapter}{\numberline {5}CONCLUS\IeC {\~O}ES E TRABALHO FUTUROS}{18}
\contentsline {chapter}{Refer\^encias Bibliogr\'aficas}{19}
